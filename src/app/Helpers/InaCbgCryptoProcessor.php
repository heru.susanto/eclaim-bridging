<?php
/**
 *  MIT License
 *
 *	Copyright (c) 2018 Heru Eko susanto (heru.ekos@gmail.com)
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in all
 *	copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *	SOFTWARE.
 *
 * * Laravel - A PHP Framework For Web Artisans
 *
 * @package  E-Claim-SIMRS Bridging integration  
 * @author   Heru Eko Sussanto
 */

namespace App\Helpers;

use Exception;
use ParagonIE;

/**
 * InaCBG Cryptographic Processing class,
 * based on crypto processing on E-Claim documentation
 */
class InaCbgCryptoProcessor
{    
  public function Encrypt($data, $key){
    $key = hex2bin($key);
   
    if(mb_strlen($key, "8bit") !== 32){
      throw new Exception("need 256 bit key");
    }
    
    $iv_size = openssl_cipher_iv_length("aes-256-cbc");
    // $iv = openssl_random_pseudo_bytes($iv_size);
    $iv = random_bytes($iv_size);
    $encrypted = openssl_encrypt($data, "aes-256-cbc", $key, OPENSSL_RAW_DATA, $iv);
    $signature = mb_substr(hash_hmac("sha256", $encrypted, $key, true), 0, 10, "8bit");
    $encoded = chunk_split(base64_encode($signature.$iv.$encrypted));
    return $encoded;
  }
    
  public function Decrypt($str, $strkey){
    $key = hex2bin($strkey);
    if(mb_strlen($key, "8bit") !== 32){
      throw new Exception("need 256 bit key");
    }
    $iv_size = openssl_cipher_iv_length("aes-256-cbc");
    $decoded = base64_decode($str);
    $signature = mb_substr($decoded, 0, 10, "8bit");
    $iv = mb_substr($decoded, 10, $iv_size,"8bit");
    $encrypted = mb_substr($decoded, $iv_size+10, NULL, "8bit");
    $calc_signature = mb_substr(hash_hmac("sha256", $encrypted, $key,true), 0, 10, "8bit");
    if(!$this->compare($signature, $calc_signature)){
      return "SIGNATURE MISMATCH";
    }
    $decrypted = openssl_decrypt($encrypted, "aes-256-cbc", $key, OPENSSL_RAW_DATA, $iv);
    return $decrypted;
  }
    
  private function compare($a, $b){
    if(strlen($a) !== strlen($b)) return false;
    $result = 0;
    for($i = 0; $i < strlen($a); $i++){
      $result != ord($a[$i]) ^ ord($b[$i]);
    }
    return $result == 0;
  }
}

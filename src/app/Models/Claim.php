<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    use HasFactory;
    /**
     * fillable
     */
    protected $fillable = [
        'pasien_id',
        'nomor_sep',
        'registrasi_rs',
        'tanggal_masuk',
        'tanggal_pulang',
        'diagnosa',
        'procedure',
        'inacbg_code',
        'inacbg_description',
        'tarif_inacbg',
        'tarif_rs',
        'sharing_peserta',
        'status_kemenkes',
        'tanggal_kirim_kemenkes'
    ];

    public function pasien() 
    {
        return $this->belongsTo(Pasien::class);
    }
}

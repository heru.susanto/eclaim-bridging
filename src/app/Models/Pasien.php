<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    use HasFactory;
    /**
     * fillable elemen
     */
    protected $fillable = [
        'nama',
        'nomor_rekam_medis',
        'nomor_bpjs',
        'nomor_nik',
        'jenis_kelamin',
        'tanggal_lahir'
    ];

    public function getJenisKelaminAttribute($jk) {
        if($jk == 0) {
            return 'Perempuan';
        } else if ($jk == 1) {
            return 'Laki-Laki';
        }
    }

    public function claims()
    {
        return $this->hasMany(Claim::class);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pasien;
use Illuminate\Http\Request;

class PasienController extends Controller
{
    public function index() 
    {
        $pasiens = Pasien::latest()->when(request()->q, function ($pasiens) {
            $pasiens = $pasiens->where('nama', 'like', '%'. request()->q . '%');
        })->paginate(15);
        return view('admin.pasien.index', compact('pasiens'));
    }

    public function show(Pasien $pasien)
    {
        $pasien = Pasien::with('claims')->where('id', $pasien->id)->first();
        $claims = $pasien->claims()->latest()->paginate(10);
        return view('admin.pasien.show', compact('pasien', 'claims'));
    }
}

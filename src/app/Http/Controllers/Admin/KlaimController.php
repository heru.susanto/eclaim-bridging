<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use Illuminate\Http\Request;

class KlaimController extends Controller
{
    public function index()
    {
        $klaims = Claim::with('pasien')->latest()->when(request()->q, function ($klaims){
            $klaims = $klaims->where('nomor_sep', 'like', '%'. request()->q . '%');
        })->paginate(15);
        return view('admin.klaim.index', compact('klaims'));
    }
}

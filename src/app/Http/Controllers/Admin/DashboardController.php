<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Claim as Claim;
use App\Models\Pasien;
use App\Models\User;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() 
    {
        $users = User::count();
        $pasiens = Pasien::count();
        $total_inacbg = Claim::where('status_kemenkes', 'sent')->sum('tarif_inacbg');
        $total_rs = Claim::where('status_kemenkes', 'sent')->sum('tarif_rs');
        $sharing = Claim::where('status_kemenkes', 'sent')->sum('sharing_peserta');
        return view('admin.dashboard.index', compact('users', 'pasiens', 'total_inacbg', 'total_rs', 'sharing'));
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\InaCbgCryptoProcessor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EKlaimController extends Controller
{
    protected $key = '';//'887b8ae3a373c36fcbace38e295d54a7181411e26f86deef61c820b1b9c1c040';
	protected $url = '';//"http://localhost/e-klaim/ws.php?mode=debug";
	protected $kode_tarifrs = '';
	protected $payor_id;
	protected $payor_cd;
	protected $processor;

	public function __construct()
	{
		$this->url = config('app.e-klaim-url') .'/ws.php';
		$this->key = config('app.e-klaim-key');
		$this->kode_tarifrs = config('app.e-klaim-kode-tarifrs');
		$this->payor_id = config('app.e-klaim-payorid');
		$this->payor_cd = config('app.e-klaim-payorcd');
		$this->processor = new InaCbgCryptoProcessor();
	}

	/**
	 * @OA\Post(
	 *   path="/claim/new",
	 *   summary="Pengajuan klaim baru sekaligus menambah data pasien jikan belum ada di database E-Klaim",
	 *   description="Pengajuan klaim baru",
	 *   operationId="NewClaim",
	 *   @OA\Parameter(
	 *     name="Claim",
	 *     in="query",
	 *     description="parameter untuk create claim",
	 *     required=true,	      
	 * 	   @OA\Schema(ref="#/models/Claim"),	     
	 *   ),
	 *   @OA\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  ),
	 *   @OA\Response(response=406, description="not acceptable"),
	 *   @OA\Response(response=500, description="internal server error")
	 * )
	 *
	 */
	public function NewClaim(Request $request)
	{	
		$q["metadata"]["method"] = "new_claim";	
		$q["data"]["nomor_sep"] = $request->nomor_sep;
		$q["data"]["nomor_kartu"] = $request->nomor_kartu;
		$q["data"]["nomor_rm"] = $request->nomor_rm;
		$q["data"]["nama_pasien"] = $request->nama_pasien;
		$q["data"]["tgl_lahir"] = $request->tanggal_lahir;
		$q["data"]["gender"] = $request->jenis_kelamin;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 * 	path="/claim/generateclaimnumber",
	 * 	summary="generate claim number for covid suspect patient",
	 * 	description="Generate SEP like number for covid suspect patient",
	 * 	produces={"application/json"},
	 * 	@SWG\Response(
	 * 		response=200,
	 * 		description="successfull generate number"
	 * 	),
	 * 	@SWG\Response(response=406, description="not acceptable"),
	 * 	@SWG\Response(response=500, description="internal server error")
	 * )
	 */
	public function generate_claim_number() 
	{
		$a['metadata']['method'] = 'generate_claim_member';
		$a['data']['payor_id'] = "71";
		$response = $this->EclaimProcess($a);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/pasien/update",
	 *   summary="Update data pasien untuk pengajuan klaim ke E-Klaim",
	 *   description="update data pasien",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="UpdatePasien",
	 *   @SWG\Parameter(
	 *     name="Claim",
	 *     in="body",
	 *     description="Data klaim",
	 *     required=true,
	 *     @SWG\Schema(ref="#/definitions/Claim"),
	 *   ),
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  ),
	 *   @SWG\Response(response=406, description="not acceptable"),
	 *   @SWG\Response(response=500, description="internal server error")
	 * )
	 *
	 */
	public function UpdatePasien(Request $request)
	{			
		$q["metadata"]["method"] = "update_patient";
		$q["metadata"]["nomor_rm"] = $request->nomor_rm;
		// $q["data"]["nomor_sep"] = $request->nomor_sep;
		$q["data"]["nomor_kartu"] = $request->nomor_kartu;
		$q["data"]["nomor_rm"] = $request->nomor_rm;
		$q["data"]["nama_pasien"] = $request->nama_pasien;
		$q["data"]["tgl_lahir"] = $request->tanggal_lahir;
		$q["data"]["gender"] = $request->jenis_kelamin;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}


	/**
	 * @SWG\Post(
	 *   path="/claim/pasien/delete",
	 *   summary="Hapus data pasien dari E-Klaim Database",
	 *   description="hapus data pasien",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="DeletePasien",	 
	 *   @SWG\Parameter(
	 *     name="DeletePasien",
	 *     in="body",
	 *     description="menghapus data.",
	 *     required=true,
	 *     @SWG\Schema(ref="#/definitions/DeletePasien"),
	 *   ),
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  )	
	 * )
	 *
	 */
	public function DeletePasien(Request $request)
	{	
		$q["metadata"]["method"] = "delete_patient";
		$q["data"]["nomor_rm"] = $request->nomor_rm;
		$q["data"]["coder_nik"] = $request->coder_nik;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/{nomor_sep}/data",
	 *   summary="Entri atau update data",
	 *   description="Entri atau update data klaim baru, jika data sudah ada maka proses ini akan melakukan update data. Parameter yang harus di isi adalah nomor SEP dan NIK Coder",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimData",
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 *     description="nomor sep yang akan di insert/update data klaimnya.",
	 *     required=true,
	 * 	   type="string"
	 *   ),
	 *   @SWG\Parameter(
	 *     name="ClaimData",
	 *     in="body",
	 *     description="data klaim",
	 *     required=true,
	 *     type="string",
	 * 	   @SWG\Schema(ref="#/definitions/ClaimData"),	     
	 *   ),
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  ),
	 *   @SWG\Response(response=406, description="not acceptable"),
	 *   @SWG\Response(response=500, description="internal server error")
	 * )
	 *
	 */
	public function ClaimData(Request $request, $nomor_sep)
	{			
		$q["metadata"]["method"] = "set_claim_data";
		$q["metadata"]["nomor_sep"] = $nomor_sep;

		$q["data"]["nomor_sep"] = $nomor_sep;
		if(!empty($request->input('nomor_kartu'))){
			$nomor_kartu = $request->input('nomor_kartu');
			$q["data"]["nomor_kartu"] = "$nomor_kartu";
		}
		if(!empty($request->input('tanggal_masuk'))){
			$tanggal_masuk = $request->input('tanggal_masuk');
			$q["data"]["tgl_masuk"] = "$tanggal_masuk";
		}
		if(!empty($request->input('tanggal_keluar'))){
			$tanggal_keluar = $request->input('tanggal_keluar');
			$q["data"]["tgl_pulang"] = "$tanggal_keluar";
		}
		if(!empty($request->input('jenis_rawat'))){
			$jenis_rawat = $request->input('jenis_rawat');
			$q["data"]["jenis_rawat"] = "$jenis_rawat";
		}
		if(!empty($request->input('kelas_rawat'))){
			$kelas_rawat = $request->input('kelas_rawat');
			$q["data"]["kelas_rawat"] = "$kelas_rawat";
		}
		if(!empty($request->input('adl_sub_acute'))){
			$adl = $request->input('adl_sub_acute');
			$q["data"]["adl_sub_acute"] = "$adl";
		}
		if(!empty($request->input('adl_chronic'))){
			$adl = $request->input('adl_chronic');
			$q["data"]["adl_chronic"] = "$adl";
		}
		if(!empty($request->input('icu'))){
			$icu = $request->input('icu');
			$q["data"]["icu_indikator"] = "$icu";
		}
		if(!empty($request->input('icu_los'))){
			$icu_los = $request->input('icu_los');
			$q["data"]["icu_los"] = "$icu_los";
		}
		if(!empty($request->input('ventilator_hour'))){
			$venti = $request->input('ventilator_hour');
			$q["data"]["ventilator_hour"] = "$venti";
		}
		if(!empty($request->upgrade_class) && !empty($request->upgrade_class_class) && !empty($request->upgrade_class_los) && !empty($request->payment_pct)){
			$upgrade_class_ind = $request->upgrade_class;
			$q["data"]["upgrade_class_ind"] = "$upgrade_class_ind";
			
			$q["data"]["upgrade_class_class"] = $request->upgrade_class_class;
			
			$los = $request->upgrade_class_los;
			$q["data"]["upgrade_class_los"] = "$los";

			$pct = $request->payment_pct;
			$q["data"]["add_payment_pct"] = "$pct";
		}
		if(!empty($request->input('birth_weight'))){
			$weight = $request->input('birth_weight');
			$q["data"]["birth_weight"] = "$weight";
		}
		if(!empty($request->input('discharge_status'))){
			$discharge = $request->input('discharge_status');
			$q["data"]["discharge_status"] = "$discharge";
		}
		if(!empty($request->input('diagnosa'))){	
			$diag = $request->input('diagnosa');
			$q["data"]["diagnosa"] = "$diag";
		}
		if(!empty($request->input('procedure')))
			$q["data"]["procedure"] = $request->input('procedure');		

		if(!empty($request->input('tarif_rs.prosedur_non_bedah'))){
			$prosedure_non_bedah = $request->input('tarif_rs.prosedur_non_bedah');
			$q["data"]["tarif_rs"]["prosedur_non_bedah"] ="$prosedure_non_bedah";
		}

		if(!empty($request->input('tarif_rs.prosedur_bedah'))){
			$procedure_bedah = $request->input('tarif_rs.prosedur_bedah');
			$q["data"]["tarif_rs"]["prosedur_bedah"] = "$procedure_bedah";
		}
		if(!empty($request->input('tarif_rs.konsultasi'))){
			$konsultasi = $request->input('tarif_rs.konsultasi');
			$q["data"]["tarif_rs"]["konsultasi"] = "$konsultasi";
		}
		if(!empty($request->input('tarif_rs.tenaga_ahli'))){
			$tenaga_ahli = $request->input('tarif_rs.tenaga_ahli');
			$q["data"]["tarif_rs"]["tenaga_ahli"] = "$tenaga_ahli";
		}
		if(!empty($request->input('tarif_rs.keperawatan'))){
			$keperawatan = $request->input('tarif_rs.keperawatan');
			$q["data"]["tarif_rs"]["keperawatan"] = "$keperawatan";
		}
		if(!empty($request->input('tarif_rs.penunjang'))){
			$penunjang = $request->input('tarif_rs.penunjang');
			$q["data"]["tarif_rs"]["penunjang"] = "$penunjang";
		}
		if(!empty($request->input('tarif_rs.radiologi'))){
			$radiologi = $request->input('tarif_rs.radiologi');
			$q["data"]["tarif_rs"]["radiologi"] = "$radiologi";
		}
		if(!empty($request->input('tarif_rs.laboratorium'))){
			$lab =  $request->input('tarif_rs.laboratorium');
			$q["data"]["tarif_rs"]["laboratorium"] = "$lab";
		}
		if(!empty($request->input('tarif_rs.pelayanan_darah')))	{
			$darah = $request->input('tarif_rs.pelayanan_darah');
			$q["data"]["tarif_rs"]["pelayanan_darah"] = "$darah";
		}
		if(!empty($request->input('tarif_rs.rehabilitasi'))){
			$rehab = $request->input('tarif_rs.rehabilitasi');
			$q["data"]["tarif_rs"]["rehabilitasi"] = "$rehab";
		}
		if(!empty($request->input('tarif_rs.kamar')))
		{
			$kamar = $request->input('tarif_rs.kamar');
			$q["data"]["tarif_rs"]["kamar"] = "$kamar";
		}
		if(!empty($request->input('tarif_rs.rawat_intensif'))){
			$intensif = $request->input('tarif_rs.rawat_intensif');
			$q["data"]["tarif_rs"]["rawat_intensif"] = "$intensif";
		}
		if(!empty($request->input('tarif_rs.obat'))){	
			$obat =  $request->input('tarif_rs.obat');
			$q["data"]["tarif_rs"]["obat"] = "$obat";
		}
		if(!empty($request->input('tarif_rs.alkes'))){
			$alkes = $request->input('tarif_rs.alkes');
			$q["data"]["tarif_rs"]["alkes"] = "$alkes";
		}	
			
		if(!empty($request->input('tarif_rs.bmhp'))){
			$bmhp =  $request->input('tarif_rs.bmhp');
			$q["data"]["tarif_rs"]["bmhp"] = "$bmhp";
		}
		// tambahan covid
		if(!empty($request->input("pemulasaraan_jenazah"))) {
			$q['data']['pemulasaraan_jenazah'] = $request->input('pemulasaraan_jenazah');
		}

		if(!empty($request->input("kantong_jenazah"))) {
			$q['data']['kantong_jenazah'] = $request->input('kantong_jenazah');
		}

		if(!empty($request->input("peti_jenazah"))) {
			$q['data']['peti_jenazah'] = $request->input('peti_jenazah');
		}
		if(!empty($request->input("plastik_erat"))) {
			$q['data']['plastik_erat'] = $request->input('plastik_erat');
		}
		if(!empty($request->input("desinfektan_jenazah"))) {
			$q['data']['desinfektan_jenazah'] = $request->input('desinfektan_jenazah');
		}
		if(!empty($request->input("mobil_jenazah"))) {
			$q['data']['mobil_jenazah'] = $request->input('mobil_jenazah');
		}
		if(!empty($request->input("desinfektan_mobil_jenazah"))) {
			$q['data']['desinfektan_mobil_jenazah'] = $request->input('desinfektan_mobil_jenazah');
		}
		if(!empty($request->input("covid19_status_cd"))) {
			$q['data']['covid19_status_cd'] = $request->input('covid19_status_cd');
		}
		if(!empty($request->input("nomor_kartu_t"))) {
			$q['data']['nomor_kartu_t'] = $request->input('nomor_kartu_t');
		}
		if(!empty($request->input("episodes"))) {
			$q['data']['episodes'] = $request->input('episodes');
		}
		if(!empty($request->input("covid19_cc_ind"))) {
			$q['data']['covid19_cc_ind'] = $request->input('covid19_cc_ind');
		}
		// end
		if(!empty($request->input('tarif_rs.sewa_alat'))){
			$sewa =  $request->input('tarif_rs.sewa_alat');
			$q["data"]["tarif_rs"]["sewa_alat"] = "$sewa";
		}
			
		if(!empty($request->input('tarif_poli_eks'))){
			$poli_eks = $request->input('tarif_poli_eks');
			$q["data"]["tarif_poli_eks"] = "$poli_eks";
		}

		if(!empty($request->input('nama_dokter')))
		{
			$dokter = $request->input('nama_dokter');
			$q["data"]["nama_dokter"] =  "$dokter";
		}
		
		$q["data"]["kode_tarif"] = $this->kode_tarifrs; // untuk iskak B Pemerintah di hardcode $request->kode_tarif;
		$q["data"]["payor_id"] = $this->payor_id;
		$q["data"]["payor_cd"] = $this->payor_cd;

		if(!empty($request->input('cob_cd')))
		{
			$cod = $request->input('cob_cd');
			$q["data"]["cob_cd"] = "$cod";
		}
			
		$q["data"]["coder_nik"] = $request->input('coder_nik'); /** mandatory */
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/{nomor_sep}/delete",
	 *   summary="Hapus data pengajuan Klaim",
	 *   description="Menghapus data pengajuan klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimDelete",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 *     description="nomor sep yang akan dihapus pengajuan klaimnya.",
	 *     required=true,
	 * 	   type="string"
	 *   ),
	 *   @SWG\Parameter(
	 *     name="DeleteClaim",
	 *     in="body",
	 *     description="menghapus data klaim.",
	 *     required=true,
	 *     @SWG\Schema(ref="#/definitions/DeleteClaim"),
	 *   ),
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success delete claim",
	 * 		@SWG\Schema(
	 * 			@SWG\Property(
	 * 				type="object",
	 * 				property="metadata",
	 * 				@SWG\Property(
	 * 					property="code",
	 * 					type="string"
	 * 				),
	 * 				@SWG\Property(
	 * 					property="message",
	 * 					type="string"
	 * 				),
	 * 			)
	 * 		)
	 *   ),
	 *   @SWG\Response(
	*      response=201,
	*      description="Failed delete process",
	*      @SWG\Schema(
	*          @SWG\Property(
	*              type="object",
	*              property="metadata",			   
	*          		@SWG\Property(
	*              		property="code",
	*              		type="string"	
	*          		),
	*				@SWG\Property(
	*              		property="message",
	*              		type="string"
	*          		),	
	*				@SWG\Property(
	*              		property="error_no",
	*              		type="string"
	*          		),
	*          ),         
	*      )
	*   )
	 * )
	 *
	 */
	public function ClaimDelete(Request $request, $nomor_sep)
	{			
		$q["metadata"]["method"] = "delete_claim";		
		$q["data"]["nomor_sep"] = $nomor_sep;
		$q["data"]["coder_nik"] = $request->coder_nik;		
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Get(
	 *   path="/claim/{nomor_sep}/print",
	 *   summary="Cetak sep  pengajuan Klaim",
	 *   description="mencetak SEP pengajuan klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimPrint",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 *     description="nomor sep yang akan cetak pengajuan klaimnya.",
	 *     required=true,
	 * 	   type="string"
	 *   ),	
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan detail claim",
	 * 		@SWG\Schema(
	 * 			@SWG\Property(
	 * 				type="object",
	 * 				property="metadata",
	 * 				@SWG\Property(
	 * 					property="code",
	 * 					type="string"
	 * 				),
	 * 				@SWG\Property(
	 * 					property="message",
	 * 					type="string"
	 * 				),
	 * 			),
	 * 			@SWG\Property(
	 * 				type="string",
	 * 				description="Base64 string for pdf file",
	 * 				property="data"
	 * 			)
	 * 		)
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function ClaimPrint(Request $request, $nomor_sep)
	{			
		$q["metadata"]["method"] = "claim_print";		
		$q["data"]["nomor_sep"] = $nomor_sep;		
		
		$response = $this->EclaimProcess($q);
		if(isset($response["data"]))
		{
			$pdf = base64_decode($response["data"]);
			file_put_contents("klaim.pdf", $pdf);
			header("Content-type:application/pdf");
			header("Content-Disposition:attachment;filename='klaim.pdf'");
			return $pdf;	
		}else{
			return response()->json($response);
		}
			
	}


	/**
	 * @SWG\Post(
	 *   path="/claim/{nomor_sep}/{coder_nik}/final",
	 *   summary="Finalisasi Klaim",
	 *   description="Finalisasi pengajuan klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimFinal",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 * 	   type="string",
	 *     description="Nomor SEP yang akan difinalisasi.",
	 *     required=true	 *     
	 *   ),
	 *   @SWG\Parameter(
	 *     name="coder_nik",
	 *     in="path",
	 * 	   type="string",
	 *     description="NIK Coder yang melakukan proses finalisasi.",
	 *     required=true	      
	 *   ),
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  )	
	 * )
	 *
	 */
	public function ClaimFinal(Request $request, $nomor_sep, $coder_nik)
	{			
		$q["metadata"]["method"] = "claim_final";		
		$q["data"]["nomor_sep"] = $nomor_sep;
		$q["data"]["coder_nik"] = $coder_nik;
				
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/{nomor_sep}/edit",
	 *   summary="Edit ulang Klaim",
	 *   description="Edit ulang klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimEdit",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 * 	   type="string",
	 *     description="Nomor SEP yang akan diedit ulang.",
	 *     required=true	     
	 *   ),	 *   
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  )	
	 * )
	 *
	 */
	public function ClaimEdit(Request $request, $nomor_sep)
	{		
		$q["metadata"]["method"] = "reedit_claim";		
		$q["data"]["nomor_sep"] = $request->nomor_sep;		
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}


	/**
	 * @SWG\Post(
	 *   path="/claim/send/collective",
	 *   summary="Pengiriman klaim kolektif",
	 *   description="Pengiriman data klaim secara kolektif berdasarkan tanggal dan jenis pelayanan",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimCollectiveSend",	 
	 *   @SWG\Parameter(
	 *     name="CollectiveClaim",
	 *     in="body",
	 *     description="parameter Data klaim yang akan dikirim.",
	 *     required=true,
	 *     @SWG\Schema(ref="#/definitions/CollectiveClaim"),
	 *   ),
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan data klaim berdasarkan nomor sep",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function ClaimCollectiveSend(Request $request)
	{			
		$q["metadata"]["method"] = "send_claim";
		
		$mulai = $request->input('tanggal_mulai');
		$q["data"]["start_dt"] = "$mulai";
		$selesai = $request->input('tanggal_selesai');
		$q["data"]["stop_dt"] = "$selesai";
		
		if(!empty($request->input('jenis_rawat'))){
			$jenis_rawat = $request->input('jenis_rawat');			
		}else{
			$jenis_rawat = 3;
		}
		$q["data"]["jenis_rawat"] = "$jenis_rawat";

		if(!empty($request->input('tipe_tanggal'))){
			$tipe = $request->input('tipe_tanggal');
		}else{
			$tipe = 1;
		}
		$q["data"]["date_type"] = "$tipe";
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/send/individual/{nomor_sep}",
	 *   summary="Pengiriman individual data klaim ke DC",
	 *   description="Pengiriman individual data klaim ke DC",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="ClaimIndividualSend",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 * 	   type="string",
	 *     description="nomor sep yang akan di kirim ke DC.",
	 *     required=true
	 *   ),
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan data klaim berdasarkan nomor sep",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function ClaimIndividualSend(Request $request, $nomor_sep)
	{	
		
		$q["metadata"]["method"] = "send_claim_individual";		
		$q["data"]["nomor_sep"] = $nomor_sep;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/claim/report/period",
	 *   summary="Cek data klaim",
	 *   description="Pengecekan data klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="IndividualClaimReport",	 
	 *   @SWG\Parameter(
	 *     name="ClaimReportRequest",
	 *     in="body",
	 *     description="query report berdasarkan tanggal dan jenis pelayanan.",
	 *     required=true,
	 *     @SWG\Schema(ref="#/definitions/ClaimReportRequest"),
	 *   ),
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan data klaim berdasarkan nomor sep",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function ClaimReport(Request $request)
	{	
		$q["metadata"]["method"] = "pull_claim";		
		$start = $request->input('tanggal_mulai');
		$q["data"]["start_dt"] = "$start";
		$end = $request->input('tanggal_selesai');
		$q["data"]["stop_dt"] = "$end";
		$jenis = $request->input('jenis_rawat');
		$q["data"]["jenis_rawat"] = "$jenis";
		
		//$response = $this->EclaimProcess($q);
		$response = $this->EclaimProcess($q);
	
		if(isset($response["data"]))
		{
			$csv = $response["data"];
			file_put_contents("klaim.csv", $csv);
			header("Content-type:application/csv");
			header("Content-Disposition:attachment;filename='klaim.csv'");
			return $csv;	
		}else{
			return response()->json($response);
		}
		return response()->json($response);
	}

	/**
	 * @SWG\Get(
	 *   path="/claim/{nomor_sep}/data",
	 *   summary="Cek data klaim",
	 *   description="Pengecekan data klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="IndividualClaimReport",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 *     description="nomor sep.",
	 *     required=true,
	 * 	   type="string"
	 *   ),	
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan data klaim berdasarkan nomor sep",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function IndividualClaimReport(Request $request, $nomor_sep)
	{			
		$q["metadata"]["method"] = "get_claim_data";		
		$q["data"]["nomor_sep"] = $nomor_sep;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Get(
	 *   path="/claim/{nomor_sep}/status",
	 *   summary="Cek status klaim",
	 *   description="Pengecekan status klaim pada BPJS",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="BPJSClaimStatus",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 *     description="nomor sep.",
	 *     required=true,
	 * 	   type="string"
	 *   ),	
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan detail procedure",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function BPJSClaimStatus(Request $request, $nomor_sep)
	{			
		$q["metadata"]["method"] = "get_claim_status";		
		$q["data"]["nomor_sep"] = $nomor_sep;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}


	/**
	 * @SWG\Get(
	 *   path="/procedure/{procedure}",
	 *   summary="Pencarian procedure",
	 *   description="pencarian procedure",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="SearchProcedure",	 
	 *   @SWG\Parameter(
	 *     name="procedure",
	 *     in="path",
	 *     description="Nama atau kode procdure.",
	 *     required=true,
	 * 	   type="string"
	 *   ),	
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan detail procedure",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function SearchProcedure(Request $request, $procedure)
	{			
		$q["metadata"]["method"] = "search_procedures";
		$q["data"]["keyword"] = $procedure;				
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Get(
	 *   path="/diagnosa/{diagnosa}",
	 *   summary="Pencarian ICD diagnosa",
	 *   description="pencarian diagnosa",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="SearchDiagnosa",	 
	 *   @SWG\Parameter(
	 *     name="diagnosa",
	 *     in="path",
	 *     description="Nama atau kode diagnosa.",
	 *     required=true,
	 * 	   type="string"
	 *   ),	
	 *   @SWG\Response(
	 * 		response=200,
	 * 		description="Success mendapatkan detail diagnosa",	
	 *   ),	
	*   )
	 * )
	 *
	 */
	public function SearchDiagnosa(Request $request, $diagnosa)
	{			
		$q["metadata"]["method"] = "search_diagnosis";
		$q["data"]["keyword"] = $diagnosa;	
		Log::debug($q);
		$response = $this->EclaimProcess($q);
		Log::debug($response);
		return response()->json($response);
	}

	/**
	 * Workhorse to e-klaim api
	 * this function processed encode request, send request to e-claim server
	 * and decode result from e-claim server
	 */
	private function EclaimProcess($request)
	{		
		$json_req = json_encode($request);
		$payload = $this->processor->Encrypt($json_req, $this->key);
		
		$headers = array("Content-Type: application/x-www-form-urlencoded");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		
		$response = curl_exec($ch);		
		$first = strpos($response, "\n") + 1;
		$last = strrpos($response, "\n") - 1;
		$response = substr($response, $first, strlen($response) - $first - $last);
		$response = $this->processor->Decrypt($response, $this->key);	
		Log::debug('response from e-claim');
		Log::debug($this->url);
		Log::debug($response);	
		$response = json_decode($response, true);
		return $response;
	}

	/**
	 * @SWG\Post(
	 *   path="/grouper/{nomor_sep}/one",
	 *   summary="Grouping 1",
	 *   description="Grouping pertama klaim",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="GrouperOne",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 * 	   type="string",
	 *     description="Nomor SEP yang akan grouper.",
	 *     required=true	     
	 *   ),	  
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  )	
	 * )
	 *
	 */
	public function GrouperOne(Request $request, $nomor_sep)
	{	
		$q["metadata"]["method"] = "grouper";
		$q["metadata"]["stage"] = "1";
		$q["data"]["nomor_sep"] = $nomor_sep;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}

	/**
	 * @SWG\Post(
	 *   path="/grouper/{nomor_sep}/two/{cmg}",
	 *   summary="Grouping 1",
	 *   description="Grouping kedua klaim jika pada hasil grouper 1 muncul kode special_cmg_option",
	 * 	 consumes={"application/json"},
	 * 	 produces={"application/json"},
	 *   operationId="GrouperTwo",	 
	 *   @SWG\Parameter(
	 *     name="nomor_sep",
	 *     in="path",
	 * 	   type="string",
	 *     description="Nomor SEP yang akan grouper.",
	 *     required=true	     
	 *   ),	 
	 *   @SWG\Parameter(
	 *     name="cmg",
	 *     in="path",
	 * 	   type="string",	 
	 *     description="Kode special cmg, jika lebih dari 1 gabung dengan karakater #.",
	 *     required=true	     
	 *   ),	  
	 *   @SWG\Response(
	 * 		response=200, 
	 * 		description="successful operation", 		
	 * 	  )	
	 * )
	 *
	 */
	public function GrouperTwo(Request $request, $nomor_sep, $cmg)
	{			
		$q["metadata"]["method"] = "grouper";
		$q["metadata"]["stage"] = "2";
		$q["data"]["nomor_sep"] = $nomor_sep;
		$q["data"]["special_cmg"] = $cmg;
		
		$response = $this->EclaimProcess($q);
		return response()->json($response);
	}
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Claim;
use App\Models\Pasien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClaimController extends Controller
{
    public function index()
    {
        $claims = Claim::with('pasien')->when(
            request()->q, function ($claims) {
                $claims = $claims->where('nomor_sep', 'like', '%'. request()->q, '%');
            }
        )->latest()->paginate(15);
        return response()->json([
            'success' => true,
            'message' => 'List data claim',
            'data' => $claims
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nomor_rekam_medis' => 'required',
            'nomor_sep' => 'required',
            'nomor_bpjs' => 'required',
            'jenis_kelamin' => 'required',
            'tanggal_lahir' => 'required|date',
            'registrasi_rs' => 'required'
        ]);

        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }
        // check data pasien.
        $pasien = Pasien::firstOrNew(
            [
                'nomor_bpjs' => $request->nomor_bpjs,
                'nomor_rekam_medis' => $request->nomor_rekam_medis,
            ],
            [
                'nama' => $request->nama,
                'jenis_kelamin' => $request->jenis_kelamin,
                'nomor_nik' => $request->nomor_nik,
                'tanggal_lahir' => $request->tanggal_lahir
            ]
        );
        $pasien->save();
        
        $claim = Claim::create(array_merge(['pasien_id' => $pasien->id], request()->all()));
        return response()->json([
            'success' => true,
            'message' => 'Claim berhasil di simpan',
            'data' => $claim
        ], 201);
    }

    /**
     * show data claim
     */
    public function show($nomor_sep)
    {
        $claim = Claim::with('pasien')->where('nomor_sep', $nomor_sep)->first();
        return response()->json([
            'success' => true,
            'message' => 'Data Claim',
            'data' => $claim
        ], 200);
    }

    public function update(Request $request, $nomor_sep)
    {
        $claim = Claim::where('nomor_sep', $nomor_sep)->first();
        $validator = Validator::make($request->all(), [
            'nomor_sep' => 'required|unique:claims,nomor_sep,'. $claim->id,
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $claim->update($request->all());
        return response()->json([
            'success' => true,
            'message' => 'Data Klaim berhasil di update',
            'data' => $claim
        ], 201);
    }


}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * index user
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'message' => 'Data Profile',
            'data' => auth()->guard('api')->user(),
        ], 200);
    }

    /**
     * update user
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // get data user
        $user = User::whereId(auth()->guard('api')->user()->id)->first();
        // update with avatar
        if ($request->file('avatar')) {
            Storage::disk('local')->delete('public/users/'. basename($user->avatar));
            $image = $request->file('avatar');
            $image->storeAs('public/users', $image->hashName());

            $user->update([
                'name' => $request->name,
                'avatar' => $image->hashName()
            ]);
        }

        // update without avatar
        $user->update([
            'name' => $request->name
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Data Profile berhasil di update!!',
            'data' => $user
        ], 201);
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::whereId(auth()->guard('api')->user()->id)->first();
        $user->update([
            'password' => Hash::make($request->password)
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Password berhasil di update',
            'data' => $user
        ], 201);
    }
}

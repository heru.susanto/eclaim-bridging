@extends('layouts.app', ['title' => 'Daftar Klaim - Admin'])

@section('content')
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-300">
    <div class="container mx-auto px-6 py-8">
        <div class="p-6 bg-white rounded-md shadow-md">
            <h2 class="text-lg text-gray-700 font-semibold capitalize">INFO PASIEN</h2>
            <hr class="mt-4">
            <form class="mt-4" method="POST">
                @csrf
                @method('PUT')
                <div>
                    <label class="block">
                        <span class="text-gray-700 text-sm">Nama Lengkap</span>
                        <input type="text" name="name" value="{{ old('name') ?? $pasien->nama }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Nama Lengkap">
                        <input type="text" name="name" value="{{ old('jenis_kelamin') ?? $pasien->jenis_kelamin }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Nama Lengkap">
                        <input type="text" name="name" value="{{ old('tanggal_lahir') ?? $pasien->tanggal_lahir }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Nama Lengkap">
                        @error('name')
                        <div
                            class="inline-flex max-w-sm w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                            <div class="px-4 py-2">
                                <p class="text-gray-600 text-sm">{{ $message }}</p>
                            </div>
                        </div>
                        @enderror
                    </label>
                </div>
                <div class="mt-4">
                    <label class="block">
                        <span class="text-gray-700 text-sm">No. RM</span>
                        <input type="email" name="email" value="{{ old('nomor_rekam_medis') ?? $pasien->nomor_rekam_medis }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Alamat Email">
                        @error('email')
                        <div
                            class="inline-flex max-w-sm w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                            <div class="px-4 py-2">
                                <p class="text-gray-600 text-sm">{{ $message }}</p>
                            </div>
                        </div>
                        @enderror
                    </label>
                </div>
                <div class="mt-4">
                    <label class="block">
                        <span class="text-gray-700 text-sm">No. BPJS</span>
                        <input type="email" name="email" value="{{ old('nomor_rekam_medis') ?? $pasien->nomor_bpjs }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Alamat Email">
                        @error('email')
                        <div
                            class="inline-flex max-w-sm w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                            <div class="px-4 py-2">
                                <p class="text-gray-600 text-sm">{{ $message }}</p>
                            </div>
                        </div>
                        @enderror
                    </label>
                </div>
                <div class="mt-4">
                    <label class="block">
                        <span class="text-gray-700 text-sm">No. NIK</span>
                        <input type="email" name="email" value="{{ old('nomor_nik') ?? $pasien->nomor_nik }}"
                            class="form-input mt-1 block w-full rounded-md" placeholder="Alamat Email">
                        @error('email')
                        <div
                            class="inline-flex max-w-sm w-full bg-red-200 shadow-sm rounded-md overflow-hidden mt-2">
                            <div class="px-4 py-2">
                                <p class="text-gray-600 text-sm">{{ $message }}</p>
                            </div>
                        </div>
                        @enderror
                    </label>
                </div>
            </form>
        </div>  

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div class="inline-block min-w-full shadow-sm rounded-lg overflow-hidden">
                <table class="min-w-full table-auto">
                    <thead class="justify-between">
                        <tr class="bg-gray-600 w-full">                       
                            <th class="px-16 py-2">
                                <span class="text-white">No SEP</span>
                            </th>                       
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tgl. Masuk</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tgl. Keluar</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Diag</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Procedure</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Ina CODE</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tarif CBG</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tarif RS</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Sharing</span>
                            </th>  
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Status Terkirim</span>
                            </th>  
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Status</span>
                            </th>                            
                        </tr>
                    </thead>
                    <tbody class="bg-gray-200">
                        @forelse($claims as $klaim)
                            <tr class="border bg-white">                            
                                <td class="px-16 py-2">
                                    {{ $klaim->nomor_sep }}
                                </td>

                                <td class="px-16 py-2">
                                    {{ $klaim->registrasi_rs }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->tanggal_masuk }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->tanggal_pulang }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->diagnosa }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->procedure }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->inacbg_code }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->tarif_inacbg) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->tarif_rs) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->sharing_peserta) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->status_kemenkes }}
                                </td>                                
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white text-center p-3 rounded-sm shadow-md">
                                Data Belum Tersedia!
                            </div>
                        @endforelse
                    </tbody>
                </table>
                @if ($claims->hasPages())
                    <div class="bg-white p-3">
                        {{ $klaims->links('vendor.pagination.tailwind') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</main>
@endsection
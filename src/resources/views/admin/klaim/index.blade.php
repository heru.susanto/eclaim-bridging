@extends('layouts.app', ['title' => 'Daftar Klaim - Admin '])

@section('content')
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-300">
    <div class="container mx-auto px-6 py-8">
        <div class="flex items-center">
            <h2 class="text-lg text-gray-700 font-semibold capitalize mx-auto items-left text-left">INFO KLAIM</h2>
            <div class="relative">
                <span class="absolute inset-y-0 left-0 pl-3 flex items-center">
                    <svg class="h-5 w-5 text-gray-500" viewBox="0 0 24 24" fill="none">
                        <path
                            d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round" />
                    </svg>
                </span>
                <form action="{{ route('admin.klaim.index') }}" method="GET">
                    <input class="form-input w-full rounded-lg pl-10 pr-4" type="text" name="q" value="{{ request()->query('q') }}"
                    placeholder="Search">
                </form>
            </div>
        </div>

        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div class="inline-block min-w-full shadow-sm rounded-lg overflow-hidden">
                <table class="min-w-full table-auto">
                    <thead class="justify-between">
                        <tr class="bg-gray-600 w-full">
                            <th class="px-16 py-2">
                                <span class="text-white">Nama</span>
                            </th>
                            <th class="px-16 py-2">
                                <span class="text-white">No RM</span>
                            </th>
                            <th class="px-16 py-2">
                                <span class="text-white">No SEP</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">No. Reg. RS</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tgl. Masuk</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tgl. Keluar</span>
                            </th>
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Diag</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Procedure</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Ina CODE</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tarif CBG</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Tarif RS</span>
                            </th> 
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Sharing</span>
                            </th>  
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Status Terkirim</span>
                            </th>  
                            <th class="px-16 py-2 text-left">
                                <span class="text-white">Status</span>
                            </th>                            
                        </tr>
                    </thead>
                    <tbody class="bg-gray-200">
                        @forelse($klaims as $klaim)
                            <tr class="border bg-white">
                                <td class="px-5 py-2">
                                    {{ $klaim->pasien->nama }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->pasien->nomor_rekam_medis }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->nomor_sep }}
                                </td>

                                <td class="px-16 py-2">
                                    {{ $klaim->registrasi_rs }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->tanggal_masuk }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->tanggal_pulang }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->diagnosa }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->procedure }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->inacbg_code }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->tarif_inacbg) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->tarif_rs) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ moneyFormat($klaim->sharing_peserta) }}
                                </td>
                                <td class="px-16 py-2">
                                    {{ $klaim->status_kemenkes }}
                                </td>
                                <td class="px-16 py-2">                                   
                                    
                                </td>
                            </tr>
                        @empty
                            <div class="bg-red-500 text-white text-center p-3 rounded-sm shadow-md">
                                Data Belum Tersedia!
                            </div>
                        @endforelse
                    </tbody>
                </table>
                @if ($klaims->hasPages())
                    <div class="bg-white p-3">
                        {{ $klaims->links('vendor.pagination.tailwind') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</main>
@endsection
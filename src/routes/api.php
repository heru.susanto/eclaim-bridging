<?php

use App\Http\Controllers\Api\ClaimController;
use App\Http\Controllers\Api\EKlaimController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\ProfileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', [LoginController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/profile', [ProfileController::class, 'index']);
    Route::post('/profile', [ProfileController::class, 'update']);
    Route::post('/profile/password', [ProfileController::class, 'updatePassword']);
    Route::resource('/claim', ClaimController::class)->except(['create', 'edit']);


    Route::post('eclaim/generateclaimnumber', [EKlaimController::class, 'generate_claim_number']);
    Route::post('eclaim/new', [EKlaimController::class, 'NewClaim']);
    Route::post('eclaim/pasien/update', [EKlaimController::class, 'UpdatePasien']);
    Route::post('eclaim/pasien/delete', [EKlaimController::class, 'DeletePasien']);
    Route::post('eclaim/{nomor_sep}/data', [EKlaimController::class, 'ClaimData']);
    Route::post('eclaim/{nomor_sep}/delete', [EKlaimController::class, 'ClaimDelete']);
    Route::get('eclaim/{nomor_sep}/print', [EKlaimController::class, 'ClaimPrint']);
    Route::post('eclaim/{nomor_sep}/{coder_nik}/final', [EKlaimController::class, 'ClaimFinal']);
    Route::get('eclaim/{nomor_sep}/edit', [EKlaimController::class, 'ClaimEdit']);
    // get claim data
    Route::get('eclaim/{nomor_sep}/data', [EKlaimController::class, 'IndividualClaimReport']);
    Route::get('eclaim/{nomor_sep}/status', [EKlaimController::class,'BPJSClaimStatus']);
    Route::post('eclaim/send/collective', [EKlaimController::class, 'ClaimCollectiveSend']);
    Route::post('eclaim/send/individual/{nomor_sep}', [EKlaimController::class, 'ClaimIndividualSend']);
    Route::post('eclaim/report/period', [EKlaimController::class, 'ClaimReport']);

    Route::get('procedure/{procedure}', [EKlaimController::class, 'SearchProcedure']);
    Route::get('diagnosa/{diagnosa}', [EKlaimController::class, 'SearchDiagnosa']);

    Route::post('grouper/{nomor_sep}/one', [EKlaimController::class, 'GrouperOne']);
    Route::post('grouper/{nomor_sep}/two/{cmg}', [EKlaimController::class, 'GrouperTwo']);
});
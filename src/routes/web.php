<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\KlaimController;
use App\Http\Controllers\Admin\PasienController;
use App\Http\Controllers\Admin\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::prefix('admin')->group(function (){
    Route::group(['middleware' => 'auth'], function (){
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard.index');
        Route::get('/profile', [ProfileController::class, 'index'])->name('admin.profile.index');
        Route::get('/pasien', [PasienController::class, 'index'])->name('admin.pasien.index');
        Route::get('/pasien/{pasien}', [PasienController::class, 'show'])->name('admin.pasien.show');
        Route::get('/klaim', [KlaimController::class, 'index'])->name('admin.klaim.index');
    });
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table){
            $table->id();
            $table->string('nama');
            $table->string('nomor_rekam_medis');
            $table->string('nomor_bpjs');
            $table->string('nomor_nik', 16)->nullable();
            $table->integer('jenis_kelamin');
            $table->date('tanggal_lahir');

            $table->timestamps();
        });

        Schema::table('pasiens', function (Blueprint $table){
            $table->index('nomor_rekam_medis');
            $table->index('nomor_bpjs');
            $table->index('nomor_nik');
        });

        Schema::create('claims', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('pasien_id');
            $table->string('nomor_sep')->unique();
            $table->string('registrasi_rs');
            $table->date('tanggal_masuk')->nullable();
            $table->date('tanggal_pulang')->nullable();
            $table->string('diagnosa')->nullable();
            $table->string('procedure')->nullable();
            $table->string('inacbg_code')->nullable();
            $table->text('inacbg_description')->nullable();
            $table->unsignedInteger('tarif_inacbg')->default(0);
            $table->unsignedInteger('tarif_rs')->default(0);
            $table->unsignedInteger('sharing_peserta')->default(0);
            $table->enum('status_kemenkes', array('unsent', 'sent'))->nullable();
            $table->dateTime('tanggal_kirim_kemenkes')->nullable();
            $table->timestamps();
        });

        Schema::table('claims', function (Blueprint $table){
            $table->index('pasien_id');
            $table->index('nomor_sep');
            $table->index('registrasi_rs');
            $table->index('diagnosa');
            $table->index('inacbg_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}

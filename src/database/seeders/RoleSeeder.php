<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'slug' => Str::slug('admin'),
            'description' => 'admin role'
        ]);
        DB::table('roles')->insert([
            'name' => 'operator',
            'slug' => Str::slug('operator'),
            'description' => 'Operator Role'
        ]);
    }
}

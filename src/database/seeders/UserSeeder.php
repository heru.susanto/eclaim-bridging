<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name', 'admin')->first();
        DB::table('users')->insert([
            'name' => 'heru eko susanto',
            'email' => 'heru@gmail.com',
            'password' => Hash::make('heru@gmail.com'),
            'role_id' => $admin->id
        ]);
        $operator = Role::where('name', 'operator')->first();
        DB::table('users')->insert([
            'name' => 'krisan',
            'email' => 'krisan@gmail.com',
            'password' => Hash::make('krisan@gmail.com'),
            'role_id' => $operator->id
        ]);
    }
}

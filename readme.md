## About SIMRS E-Claim Bridging

SIMRS E-Claim Bridging merupakan aplikasi bridging SIMRS dengan aplikasi E-Claim/InaCBG P2JK Kemenkes. Aplikasi ini cocok digunakan untuk Rumah Sakit yang sudah menggunakan SIMRS namun kesulitan untuk melakukan bridging langsung ke E-Claim.

## Konfigurasi

Berikut ini merupakan langkah-langkah untuk melakukan setting aplikasi ini. SIMRS E-Claim bridging ini dibuat dengan menggunakan framework [Laravel](https://laravel.com/) versi 7.0. Dengan menggunakan [docker compose](https://docker.io).

- Pastikan aplikasi E-Claim Kemenkes sudah di install, petunjuk instalasi bisa dilihat pada [dokumentasi](https://inacbg.kemkes.go.id/index.php?XP_view=1&page=download#juknis).
- Login pada aplikasi dengan user dengan hak akses 'Pengaturan dan Pemeliharaan'. Login default untuk user dengan hak akses tersebut adalah inacbg, password: inacbg
- Lakukan setup institusi seperti kode RS dan lain sebagainya.
- Silahkan masuk ke Grup Pengaturan dan Pemeliharaan, kemudian klik Setup -> Integrasi -> SIMRS
- Jika sudah masuk ke laman "SETUP INTEGRASI SIMRS", klik tombol "Generate Key" yang ada di kanan bawah. ikuti petunjuknya hingga Encryption Key berhasil dibuat.
- Install [docker](https://docker.io/). [docker](https://docker.io/) merupakan container platform untuk menyeragamkan linkungan pengembangan aplikasi mulai tahap pengembangan, testing, sampai ke deploy. 
- Download aplikasi [SIMRS E-Klaim Bridging](https://gitlab.com/heru.susanto/eclaim-bridging/repository/master/archive.zip) atau jika anda terbiasa menggunakan GIT silahkan clone [Master](https://gitlab.com/heru.susanto/eclaim-bridging.git)
- Ekstraksi file tersebut ke sembarang folder yang ada di komputer anda.
- jalankan command prompt jika menggunakan OS Windows atau terminal jika anda menggunakan Linux/Mac Os, masuk ke folder hasil ektraksi SIMRS E-Klaim Bridging. Jalankan perintah "composer install" untuk menginstall library-library yang dibutuhkan oleh laravel. Waktu yang dibutuhkan untuk instalasi aplikasi ini sesuai dengan kecepatan koneksi internet yang digunakan.
- Copy file .env.example dan paste dengan nama .env
- Set key aplikasi dengan command php artisan key:generate
- Buka file .env dengan teks editor, ubah beberapa settings yang ada
- Sesuaikan nilai EKLAIM_URL sesuai dengan IP address komputer dimana E-Claim terinstall. jika E-Claim diakses dibrowser Mozilla/Chrome dengan alamat http://192.168.0.1/e-klaim maka nilai setting EKLAIM_URL adalah EKLAIM_URL=http://192.168.0.1/e-klaim/ws.php
- Sesuaikan EKLAIM_KEY dengan key yang sudah digenerate pada aplikasi E-Klaim tadi.
- Sesuaikan nilai EKLAIM_KODETARIFRS  Kode tarif adalah kelas tarif INA-CBG berdasarkan kelas rumah sakit dan kepemilikannya sesuai dengan kode tarip RS, lihat kembali [petunjuk instalasi E-Claim](https://inacbg.kemkes.go.id/index.php?XP_view=1&page=download#juknis) laman 63.
- Sesuaikan nilai EKLAIM_PAYORID dan EKLAIM_PAYORCD sesuai dengan [manual eklaim](https://inacbg.kemkes.go.id/index.php?XP_view=1&page=download#juknis) laman 61


## Ujicoba
- Jalankan aplikasi ini, jika anda menggunakan php command maka aplikasi ini dapat diakses pada http://localhost:8000
- Aplikasi [SIMRS E-Claim Bridging](https://gitlab.com/heru.susanto/eclaim-bridging) ini dilengkapi dengan Dokumentasi API yang mengacu pada standar [Swagger](https://swagger.io). Silahkan buka link Documentation pada homepage. Jika SIMRS E-Claim Bridging ini dijalankan pada alamat http://localhost:8000 maka dokumentasi apinya bisa diakses pada http://localhost:8000/api/documentation

## Kontribusi

Anda dapat berkontribusi pada pengembangan aplikasi ini dengan berbagai cara seperti klik star pada repository [SIMRS E-Klaim Bridging](https://gitlab.com/heru.susanto/eclaim-bridging.git),  menulis dokumentasi, melakukan ujicoba bahkan bisa bergabung untuk bersama-sama mengembangkan aplikasi ini. Jika anda berkenan untuk mengirim donasi untuk pengembangan aplikasi ini silahkan bersurat ke Heru Eko Susanto [heru.ekos@gmail.com]. 

Kami sangat mengapresiasi donasi anda dan akan menulis anda sebagai salah satu donatur/kontributor untuk pengembangan aplikasi ini. Oh ya jika anda kesulitan silahkan kirim email ke Heru Eko Susanto [heru.ekos@gmail.com] untuk bertanya dan sebagainya.

## Security Vulnerabilities

Jika anda menemukan celah keamanan pada aplikasi ini, silahkan kirim surat elektronik/email ke Heru Eko Susanto [heru.ekos@gmail.com]. Semua laporan anda akan kami tanggapi.

## License

SIMRS E-Claim Bridging ini merupakan aplikasi sumber kode terbuka atau open-sourced software dengan licens [MIT license](https://opensource.org/licenses/MIT).
